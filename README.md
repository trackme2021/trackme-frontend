# TrackME 

## Demo Link 

- [TrackMe](http://trackme-app-2021.s3-website.ap-south-1.amazonaws.com)
- To access the Account with demo data please login using the following Credentials
- email: johndoe@gmail.com
- password: John@123



TrackMe is Web Application that helps its user to self analyze their activities by logging in their activities through out a period of time.
Users put in their activity detail and later can see their events in a form of a story.
This App contains a Dashboard that shows the analytics of their activity in a period of time in the form of graphs and charts so that users can self analyse their growth.
## Table of Contents
- [TrackME](#trackme)

  * [Features](#features)
  * [Documents](#documents)
  * [Tech](#tech)
  * [Tools and Libraries](#tools)
  * [Installation](#installation)
  * [Contacts](#contacts)
  * [License](#license)




## Features

- Create your daily events and save them and see them as a story anytime.
- Search for your events by applying filter of date, tag, and seach keywords.
- Log your events for a period of time and See your analytics in the form of graphs and charts.
- Create your own tags and connect them to your events.
- User Can Login and register manually or by Social Logins( Google and Facebook )
![Login Page](https://trackme0001.s3.ap-south-1.amazonaws.com/trackme_login.png "Login")
- User Can Create Events and then later can see them on as a story,Filter by Tags and date on the All Events Page
 ![All Events](https://trackme0001.s3.ap-south-1.amazonaws.com/trackme_all_event.png "All Events Page")
- User Can Select Default made tags or can create their own custom tags and link them to Events
![Tags](https://trackme0001.s3.ap-south-1.amazonaws.com/trackme_tags.png "Tags")
- After Logging their events on the App User can go the dashboard Page by clicking on the toggle button in the Navbar to view and analyze their activities in form of graphs and charts,To get an idea of time spent on each type of acitivity(Tag) by selecting the Tag .
![Dashboard](https://trackme0001.s3.ap-south-1.amazonaws.com/trackme_dashboard.png "Dashboard")
## Documents

- [API-DOCS] - API documentation of the Application
- [UI-Wireframes]- Wireframes of the Appication
- [UI-Wireframes-mobile] - Wireframes of the Appication-Mobile
- [Overview] - One Pager document of the Application
- [PRD] - Project Requirement Document of the Project
- [Stack-justification-doc] - Stack Justification Doc

## Tech

- [ReactJS] - HTML enhanced for web apps!
- [node.js] - evented I/O for the backend
- [Express] - NodeJs Framework
- [MongoDB] - NoSQL database
- [AWS] - Static web hosting


## Tools and Libraries 

- [JIRA] - Project Management
- [Draw.io] - Wireframing
- [AWS S3] - Static Hosting
- [Cypress] - End to End testing
- [Heroku] - cloud hosting
- [Jest] - Unit testing
- [React-testing-Library] - Integration testing
- [Circle-CI] - CICD

## Prerequisites

The prerequisites that you have to install before using the app
```sh
npm install npm@latest -g
```
## Installation

TrackMe requires [Node.js](https://nodejs.org/) v10+ to run.
Checkout to develop branch
Install the dependencies and devDependencies and start the frontend server.

```sh
cd trackMe-frontend
Checkout to develop branch
git checkout develop
npm i
npm start
```

Testing
- command to run tests in terminal
```sh
cd trackMe-frontend
Checkout to develop branch
git checkout develop
npm run e2e:open
```

- command to run tests in terminal and visualize in browser
```sh
cd trackMe-frontend
Checkout to develop branch
git checkout develop
npm run e2e:open
```


## Backend Source Code
- [Backend Source Repository](https://bitbucket.org/trackme2021/trackme-backend/src)
Install the dependencies and devDependencies and start the backend server.

```sh
cd trackMe-backend
git checkout develop
npm i
npm run dev
```
- Env file for backend local setup
```sh
NODE_ENV=development
PORT=5000
MONGO_URI='mongo-uri'
GOOGLE_CLIENT_ID = 'google client id'
GOOGLE_CLIENT_SECRET = "google client secret"
API_URL = http://localhost:5000
CLIENT_URL = http://localhost:3000
EMAIL_FROM = 'testing email'
EMAIL_USER = 'testing email'
EMAIL_PASS = inkumorijbwjiizf
AWS_ACCESS_KEY_ID = 'aws access key id'
AWS_SECRET_ACCESS_KEY = 'aws secret access key'
```

## RoadMap
- Mile Stones
![Mile Stones](https://trackme0001.s3.ap-south-1.amazonaws.com/Screen+Shot+2021-06-26+at+22.12.12.png "Mile Stones")
- Jira Tickets
![Jira Tickets](https://trackme0001.s3.ap-south-1.amazonaws.com/jira_issues.PNG "Jira Tickets")

## Out of Scope
-  Firebase Notifications
-  Redis Cache 
-  Fremium Premium model
-  Progressive web application

## License

MIT

**Free Software, Hell Yeah!**



   [LinkedIn-Suyash]: <https://www.linkedin.com/in/suyash-mishra-b5913a171/>
   [AWS S3]: <https://aws.amazon.com/>
   [Cypress]: <https://www.cypress.io/>
   [MongoDB]: <https://www.mongodb.com/>
   [AWS]: <https://aws.amazon.com/>
   [node.js]: <http://nodejs.org>
[Heroku]: <https://dashboard.heroku.com/>
[JIRA]: <https://jira.com/>
[Jest]: <https://jestjs.io/>
[React-testing-Library]: <https://testing-library.com/docs/react-testing-library/intro/>
[Circle-CI]: <https://dashboard.heroku.com/>
   [express]: <http://expressjs.com>
   [Stack-justification-doc]: <https://docs.google.com/document/d/1Rg0nBpHCKMzwgAmSZtHZmXJSnOcLXA5_s7pv4pvJFKY/edit?usp=sharing>
   [ReactJS]: <http://reactjs.org>
   [API-DOCS]: <https://trackme-backend-2021.herokuapp.com/api-docs>
   [UI-Wireframes]: <https://drive.google.com/file/d/1xMzejGBZpvZ-L0mibQA4fpXHihjzfEbN/view>
   [UI-Wireframes-mobile]: <https://drive.google.com/file/d/1O_c71WYOvD9FPSEK_KAjApxtN78h1s-l/view>
   [Overview]: <https://drive.google.com/file/d/1H_MnC9FTSS_sUT-smI-dn1U8FlRHIsIn/view?usp=sharing>
   [PRD]: <https://drive.google.com/file/d/1wY1AW_AQLBW3iGjh2hF4_gigqKrRFYNk/view?usp=sharing>
   


